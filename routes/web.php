<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('fuck-u', function () {
    return view('fuck-u');
});



//Auth::routes();

Route::get('/login', function () {
    return view('auth.login');
});

Route::post('login/request', ['as' => 'login/request', 'uses' => 'Auth\LoginController@cookieLogin']);
Route::post('registerRequest', ['as' => 'registerRequest', 'uses' => 'Jira\ViewController@getRegisterView']);
//Route::get('/login', 'LoginController@cookieLogin')->name('login');

Route::get('/home', 'HomeController@index')->name('home');

<?php 
namespace App\Http\Controllers\jira;

use App\Http\Controllers\Controller;

/**
* View controller
*/
class ViewController extends RequestController
{
	public function getLoginView() {
		return view('auth.login');
	}


	public function getRegisterView() {
		return view('auth.register');
	}
}

?>
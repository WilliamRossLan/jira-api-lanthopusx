<?php 
namespace App\Http\Controllers\jira;

use App\Http\Controllers\Controller;

/**
* jira request handler
*/
class RequestController extends controller
{
	protected $api_url = "rest/api/2/";

	protected $_httpHeader = array("Content-Type: application/json");
	
	protected $_url;
	protected $_baseUrl = 'https://lanthopus-x.atlassian.net/';

	protected $_followlocation; 
	protected $_timeout; 
	protected $_maxRedirects; 
	protected $_post; 
	protected $_postFields; 
	protected $_result;

	protected $_session; 
	protected $_webpage; 
	protected $_includeHeader; 
	protected $_noBody; 
	protected $_status; 
	protected $_binaryTransfer;
	protected $_returnTransfer = true; 

	protected $_timeOut;
	protected $_cookiestr;

	protected $_curl;
	protected $_sess_arr;

	public function authorizeUser($username, $password)
	{
		// encode jsondata from input
		$jsondata = json_encode(
			array(
				'username' => $username,
				'password' => $password
			)
		);

		$this->_url = $this->_baseUrl."rest/auth/1/session";

		$this->setPostFields($jsondata);
		$this->creatCurl();
		//var_dump($this->_sess_arr);
		if(isset($this->_sess_arr['errorMessages'][0])) {
			echo $this->_sess_arr['errorMessages'][0];
		}
		else {
			setcookie('JSESSIONID', $this->_sess_arr['session']['value'], time() + (86400 * 30), "/");
			$_COOKIE['HostOnly'] = false;
			$this->_post = false;
			$this->creatCurl($this->_baseUrl.$this->api_url."issue/WWW-8.json");
			var_dump($this->_result);
		}

	}

	public function setPostFields($postFields) 
	{
		$this->_post = true;
		$this->_postFields = $postFields;
	}

	public function creatCurl($url = null) 
	{
		try {
			if ($url != null) {
				$this->_url = $url;
			}

			if ($this->_url == null) {
				throw new Exception("URL is not set.");
			}
			
		} catch (Exception $e) {
			throw $e;
		}

		$options = array(
			CURLOPT_URL => $this->_url,
			CURLOPT_TIMEOUT => $this->_timeOut,
			CURLOPT_HTTPHEADER => $this->_httpHeader,
			CURLOPT_MAXREDIRS => $this->_maxRedirects,
			CURLOPT_RETURNTRANSFER => $this->_returnTransfer,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false
		);

		$ch = curl_init();

		curl_setopt_array($ch, $options);

		if ($this->_post) 
		{
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_postFields);
		}

		if($this->_includeHeader) 
		{ 
			curl_setopt($s,CURLOPT_HEADER,true); 
		} 

		if($this->_noBody) 
		{ 
			curl_setopt($s,CURLOPT_NOBODY,true); 
		} 

		if ($this->loginCheck()) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('cookie:'.$this->_cookiestr)); 
		}

		$this->_result = curl_exec($ch);
		var_dump($this->_result);

		$this->_sess_arr = json_decode($this->_result, true);

		//var_dump($this->_sess_arr);
		$this->_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		// CLOSE CURL AFTER PROCESSING
		$ch = curl_close($ch);

	}

	public function getHttpstatus() 
	{
		return $this->_status;
	}

	public function setHttpHeader($header) {
		$this->_httpHeader = $header;
	}

	public function setBaseUrl($url) {
		$this->_baseUrl = $url;
	}

	public function loginCheck()
	{
		if(isset($_COOKIE['JSESSIONID'])){
			$this->_cookiestr='cloud.session.token='.$_COOKIE['JSESSIONID'];
			return true;
		}
		else{
			$this->_cookiestr="";
			return false;
		}
	}
} 
?>
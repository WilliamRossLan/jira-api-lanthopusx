<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use  App\Http\Controllers\jira\RequestController;

use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    private $requestController;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->requestController = new RequestController();
    }

    public function cookieLogin() {
        $this->requestController->authorizeUser(Input::get('email'), Input::get('password'));
    }

    public function isLoggedIn() {
        if (condition) {
            # code...
        }
    }
}
